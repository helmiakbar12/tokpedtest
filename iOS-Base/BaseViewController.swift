//
//  BaseViewController.swift
//  iOS-Base
//
//  Created by helmi akbar on 8/24/17.
//  Copyright © 2017 helmi akbar. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class BaseViewController: UIViewController, NVActivityIndicatorViewable {

    var mainScrollView : UIScrollView? = nil
    var tap : UITapGestureRecognizer!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func basicAlertView(title: String, message: String, successBlock: (() -> Swift.Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: {
            (alert: UIAlertAction!) in
            successBlock!()
        }
        ))
        self.present(alert, animated: true, completion: nil)
    }
    
    func hideNavigationBar() {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    func showNavigationBar() {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    
    func showLoadingIndicator() {
        startAnimating(CGSize(width: 40, height: 40), type: NVActivityIndicatorType(rawValue: 3)!)
    }
    
    func hideLoadingIndicator() {
        self.stopAnimating()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension BaseViewController{
    // MARK: Keyboard Control
    func registerKeyboardNotification(scrollView: UIScrollView) {
        self.mainScrollView = scrollView
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillShow(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillShow,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide,
                                               object: nil)
        
        tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        tap.delegate = self as? UIGestureRecognizerDelegate
        
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if self.mainScrollView == nil {
            return
        }
        
        //Need to calculate keyboard exact size due to Apple suggestions
        self.mainScrollView?.isScrollEnabled = true
        let info : NSDictionary = notification.userInfo! as NSDictionary as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize!.height, 0.0)
        
        self.mainScrollView?.contentInset = contentInsets
        self.mainScrollView?.scrollIndicatorInsets = contentInsets
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if self.mainScrollView?.getSelectedTextField() != nil
        {
            let textField = self.mainScrollView?.getSelectedTextField()
            var resultFrame = CGRect.zero
            resultFrame = self.mainScrollView?.convert((textField?.frame)!, from: textField?.superview) ?? CGRect.zero
            
            if (!aRect.contains(resultFrame))
            {
                self.mainScrollView?.scrollRectToVisible(resultFrame, animated: true)
            }
        }
        else if self.mainScrollView?.getSelectedTextView() != nil
        {
            let textView = self.mainScrollView?.getSelectedTextView()
            var resultFrame = CGRect.zero
            resultFrame = self.mainScrollView?.convert((textView?.frame)!, from: textView?.superview) ?? CGRect.zero
            
            if (!aRect.contains(resultFrame))
            {
                self.mainScrollView?.scrollRectToVisible(resultFrame, animated: true)
            }
        }
        
        
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        self.mainScrollView?.contentInset = contentInset
        self.view.removeGestureRecognizer(tap)
    }
    
    @objc func handleTap() {
        self.view.endEditing(true)
    }
    
}
