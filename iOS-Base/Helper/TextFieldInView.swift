//
//  TextFieldInView.swift
//  iOS-Base
//
//  Created by helmi akbar on 23/04/18.
//  Copyright © 2018 helmi akbar. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    func getSelectedTextField() -> UITextField? {
        
        let totalTextFields = getTextFieldsInView(view: self)
        
        for textField in totalTextFields{
            if textField.isFirstResponder{
                return textField
            }
        }
        
        return nil
        
    }
    
    func getTextFieldsInView(view: UIView) -> [UITextField] {
        
        var totalTextFields = [UITextField]()
        
        for subview in view.subviews as [UIView] {
            if let textField = subview as? UITextField {
                totalTextFields += [textField]
            } else {
                totalTextFields += getTextFieldsInView(view: subview)
            }
        }
        
        return totalTextFields
    }
    
    func getSelectedTextView() -> UITextView? {
        
        let totalTextViews = getTextViewsInView(view: self)
        
        for TextView in totalTextViews{
            if TextView.isFirstResponder{
                return TextView
            }
        }
        
        return nil
        
    }
    
    func getTextViewsInView(view: UIView) -> [UITextView] {
        
        var totalTextViews = [UITextView]()
        
        for subview in view.subviews as [UIView] {
            if let TextView = subview as? UITextView {
                totalTextViews += [TextView]
            } else {
                totalTextViews += getTextViewsInView(view: subview)
            }
        }
        
        return totalTextViews
    }
}
