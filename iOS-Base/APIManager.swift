//
//  APIManager.swift
//  iOS-Base
//
//  Created by helmi akbar on 8/24/17.
//  Copyright © 2017 helmi akbar. All rights reserved.
//

import UIKit
import Alamofire

class APIManager {
    
    private init() {}
    static let sharedInstance = APIManager()
    
    typealias SuccessHandlerObject = (_ responseObject: [String: Any]) -> Void
    typealias SuccessHandlerArray = (_ responseObject: Array<Any>) -> Void
    typealias FailHandler = (_ errorMessage: String) -> Void
    
    
    // MARK: - GET
    func GETAPIWithResponseObject(route: String, authorization: Bool, successBlock:@escaping SuccessHandlerObject, failureBlock:@escaping FailHandler){
        
        var headers: HTTPHeaders;
        if authorization {
            print("TOKEN: \(DataManager.sharedInstance.getAuthToken())")
            headers = [
                "Authorization": DataManager.sharedInstance.getAuthToken(),
                "Content-Type": "application/json"
            ]
        }else{
            
            headers = [
                "Content-Type": "application/json"
            ]
        }
        
        Alamofire.request(Constant.BaseURL+route, headers: headers).responseJSON { response in
            
            print("REQUEST.ROUTE: \(route)")
            print("header: \(headers)")
            
            switch response.result {
            case .success:
                let responseJSON = response.result.value as! [String:Any]
                
                print("RESPONSE.BODY: \(responseJSON)")
                let meta = responseJSON["meta"] as! [String:Any]
                let message = meta["message"] as! String
                let status = meta["status"] as! Int
                
                if status == 0 {
                    if message != "" {
                        failureBlock(message)
                    } else {
                        failureBlock("Unknown Error")
                    }
                } else{
                    successBlock(responseJSON)
                }
                
                break
                
            case .failure(let error):
                print("ERROR: \(error)")
                failureBlock((response.error?.localizedDescription)!)
                break
            }
        }
    }
    
    func GETAPIWithParamsAndResponseObject(parameter: [String: Any], route: String, authorization: Bool, successBlock:@escaping SuccessHandlerObject, failureBlock:@escaping FailHandler){
        
        var headers: HTTPHeaders;
        if authorization {
            print("TOKEN: \(DataManager.sharedInstance.getAuthToken())")
            headers = [
                "Authorization": DataManager.sharedInstance.getAuthToken(),
                "Content-Type": "application/json"
            ]
        }else{
            
            headers = [
                "Content-Type": "application/json"
            ]
        }
        
        let parameters: Parameters = parameter
        
        Alamofire.request(Constant.BaseURL+route, method: .get, parameters: parameters, encoding: URLEncoding.queryString, headers: headers).responseJSON { response in
            
            print("REQUEST.ROUTE: \(route)")
            
            switch response.result {
            case .success:
                let responseJSON = response.result.value as! [String:Any]
                
                print("RESPONSE.BODY: \(responseJSON)")
                let meta = responseJSON["meta"] as! [String:Any]
                let message = meta["message"] as! String
                let status = meta["status"] as! Int
                
                if status == 0 {
                    if message != "" {
                        failureBlock(message)
                    } else {
                        failureBlock("Unknown Error")
                    }
                } else{
                    successBlock(responseJSON)
                }
                
                break
                
            case .failure(let error):
                print("ERROR: \(error)")
                failureBlock((response.error?.localizedDescription)!)
                break
            }
        }
    }
    
    func GETAPIWithResponseArray(route: String, authorization: Bool, successBlock:@escaping SuccessHandlerArray, failureBlock:@escaping FailHandler){
        
        var headers: HTTPHeaders;
        if authorization {
            print("TOKEN: \(DataManager.sharedInstance.getAuthToken())")
            headers = [
                "Authorization": "Bearer " + DataManager.sharedInstance.getAuthToken(),
                "Content-Type": "application/json"
            ]
        }else{
            
            headers = [
                "Content-Type": "application/json"
            ]
        }
        
        Alamofire.request(Constant.BaseURL+route, headers: headers).responseJSON { response in
            
            print("REQUEST.ROUTE: \(route)")
            
            switch response.result {
            case .success:
                if ((response.result.value as? Array<Any>) != nil) {
                    let responseJSON = response.result.value as! Array<Any>
                    
                    print("RESPONSE.BODY: \(responseJSON)")
                    
                    successBlock(responseJSON)
                }else{
                    failureBlock("Error, Response is an Object")
                }
                
                break
                
            case .failure(let error):
                print("ERROR: \(error)")
                failureBlock((response.error?.localizedDescription)!)
                break
            }
        }
    }
    
    
    
    // MARK: - POST
    //    func POSTAPIWithResponseObject(route: String, parameter: [String: Any], successBlock:@escaping SuccessHandlerObject, failureBlock:@escaping FailHandler){
    //
    //        Alamofire.request(Constant.BaseURL+route, method: .post, parameters: parameter, encoding: JSONEncoding.default).responseJSON { response in
    //
    //            print("REQUEST.ROUTE: \(route)")
    //            print("REQUEST.BODY: \(NSString(data: (response.request?.httpBody)!, encoding: String.Encoding.utf8.rawValue))")
    //
    //            switch response.result {
    //            case .success:
    //                let responseJSON = response.result.value as! [String:Any]
    //
    //                print("RESPONSE.BODY: \(responseJSON)")
    //
    //                if responseJSON["status"] != nil {
    //                    let status = responseJSON["status"] as! Bool
    //                    if status == false {
    //                        if responseJSON["message"] != nil {
    //                            failureBlock(responseJSON["message"] as! String)
    //                        }else{
    //                            failureBlock("Unknown Error")
    //                        }
    //                    }else{
    //                        successBlock(responseJSON)
    //                    }
    //                }else {
    //                    failureBlock("Error with undefined status")
    //                }
    //                break
    //
    //            case .failure(let error):
    //                print("ERROR: \(error)")
    //                failureBlock((response.error?.localizedDescription)!)
    //                break
    //            }
    //        }
    //    }
    
    func POSTAPIWithResponseObject(route: String, authorization: Bool, parameter: [String: Any], successBlock:@escaping SuccessHandlerObject, failureBlock:@escaping FailHandler){
        
        var headers: HTTPHeaders;
        if authorization {
            print("TOKEN: \(DataManager.sharedInstance.getAuthToken())")
            headers = [
                "Authorization": "Bearer " + DataManager.sharedInstance.getAuthToken(),
                "Content-Type": "application/json"
                //                "Content-Type": "application/x-www-form-urlencoded"
            ]
        }else{
            
            headers = [
                "Content-Type": "application/json"
                //                "Content-Type": "application/x-www-form-urlencoded"
            ]
        }
        
        Alamofire.request(Constant.BaseURL+route, method: .post, parameters: parameter, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            
            print("REQUEST.ROUTE: \(route)")
            print("REQUEST.BODY: \(NSString(data: (response.request?.httpBody)!, encoding: String.Encoding.utf8.rawValue))")
            
            switch response.result {
            case .success:
                let responseJSON = response.result.value as! [String:Any]
                
                print("RESPONSE.BODY: \(responseJSON)")
                
                if responseJSON["meta"] != nil {
                    let meta = responseJSON["meta"] as! [String : Any]
                    let status = meta["status"] as! Int
                    if status == 0 {
                        if meta["message"] != nil {
                            failureBlock(meta["message"] as! String)
                        }else{
                            failureBlock("Unknown Error")
                        }
                    }else{
                        successBlock(responseJSON)
                    }
                }else {
                    successBlock(responseJSON)
                }
                break
                
            case .failure(let error):
                print("ERROR: \(error)")
                failureBlock((response.error?.localizedDescription)!)
                break
            }
        }
    }
    
    func POSTAPIWithResponseArray(route: String, authorization: Bool, parameter: [String: Any], successBlock:@escaping SuccessHandlerArray, failureBlock:@escaping FailHandler){
        
        var headers: HTTPHeaders;
        if authorization {
            print("TOKEN: \(DataManager.sharedInstance.getAuthToken())")
            headers = [
                "Authorization": "Bearer " + DataManager.sharedInstance.getAuthToken(),
                "Content-Type": "application/json"
            ]
        }else{
            
            headers = [
                "Content-Type": "application/json"
            ]
        }
        
        Alamofire.request(Constant.BaseURL+route, method: .post, parameters: parameter, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            
            print("REQUEST.ROUTE: \(route)")
            print("REQUEST.BODY: \(NSString(data: (response.request?.httpBody)!, encoding: String.Encoding.utf8.rawValue))")
            
            switch response.result {
            case .success:
                if ((response.result.value as? Array<Any>) != nil) {
                    let responseJSON = response.result.value as! Array<Any>
                    print("RESPONSE.BODY: \(responseJSON)")
                    successBlock(responseJSON)
                }else{
                    failureBlock("Error, Response is an Object")
                }
                
                break
                
            case .failure(let error):
                print("ERROR: \(error)")
                failureBlock((response.error?.localizedDescription)!)
                break
            }
        }
    }
    
    //MARK: - Delete
    func DELETEAPIWithResponseObject(route: String, authorization: Bool, parameter: [String: Any], successBlock:@escaping SuccessHandlerObject, failureBlock:@escaping FailHandler){
        
        var headers: HTTPHeaders;
        if authorization {
            print("TOKEN: \(DataManager.sharedInstance.getAuthToken())")
            headers = [
                "Authorization": "Bearer " + DataManager.sharedInstance.getAuthToken(),
                "Content-Type": "application/json"
                //                "Content-Type": "application/x-www-form-urlencoded"
            ]
        }else{
            
            headers = [
                "Content-Type": "application/json"
                //                "Content-Type": "application/x-www-form-urlencoded"
            ]
        }
        
        Alamofire.request(Constant.BaseURL+route, method: .delete, parameters: parameter, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            
            print("REQUEST.ROUTE: \(route)")
            print("REQUEST.BODY: \(NSString(data: (response.request?.httpBody)!, encoding: String.Encoding.utf8.rawValue))")
            
            switch response.result {
            case .success:
                let responseJSON = response.result.value as! [String:Any]
                
                print("RESPONSE.BODY: \(responseJSON)")
                
                if responseJSON["meta"] != nil {
                    let meta = responseJSON["meta"] as! [String : Any]
                    let status = meta["status"] as! Int
                    if status == 0 {
                        if meta["message"] != nil {
                            failureBlock(meta["message"] as! String)
                        }else{
                            failureBlock("Unknown Error")
                        }
                    }else{
                        successBlock(responseJSON)
                    }
                }else {
                    successBlock(responseJSON)
                }
                break
                
            case .failure(let error):
                print("ERROR: \(error)")
                failureBlock((response.error?.localizedDescription)!)
                break
            }
        }
    }
}
