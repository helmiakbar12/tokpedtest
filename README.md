# README #

Clapping APE iOS App Base Project Template

### SPECIFICATIONS ###

* Version 1.0
* Language: Swift 3
* Architecture: MVP
* [Learn Markdown](http://iyadagha.com/using-mvp-ios-swift/)

### DOCUMENTATION ###

## 1.Function to make text field or text view appear above keyboard ##

### Usage ###
```swift
func registerKeyboardNotification(scrollView: UIScrollView)
```

### Example ###
```swift
@IBOutlet weak var scrollView: UIScrollView!

override func viewDidLoad() {
    super.viewDidLoad()
    func registerKeyboardNotification(scrollView: scrollView)
}
```
